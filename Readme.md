# docker-elasticsearch-curator

## Purpose
If you are running an ELK or Elastic Stack you have the problem that indicies 
pile up and there is not automatic way to manage the indicies.

Curator is a tool from Elasticsearch to manage indicies in a ES cluster.
It can delete, close, open and other operations (see their website for more information)


## Configuration
See the docker-compose.yml for examples how to configure this container.

Basically this container installs curator via pip and runs a "cron" job by the 
variable `INTERVAL_IN_HOURS`. It runs all curator yaml files in curator/actions/

If you are unsure wether your actions are configured properly, you can set the 
environment variable `DRYRUN` to `TRUE` to see which indicies would be managed.

## Options

- `ES_HOST` The Elasticsearch Hostname to connect to
- `DRYRUN` Defaults to TRUE
- `INTERVAL_IN_HOURS` how long should the process wait to run again in hours.
